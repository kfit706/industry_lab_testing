package ictgradschool.industry.lab_testing.ex01;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class RobotTest {

    private Robot myRobot;

    @Before
    public void setUp() {
        myRobot = new Robot();
    }
    //if we run everytime we can clean up the test memory, start fresh every time to ensure independence of methods -> @before
    //we want every test to be independent
    //dont need to worry about the previous steps or previous test status

    @Test
    public void testRobotConstruction() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        //check that the robot is initially facing north from cell 1,10
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());

    }

    @Test
    public void testIllegalMoveNorth() {
        boolean atTop = false;
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atTop = myRobot.currentState().row == 1;
            assertTrue(atTop);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().row);
        }
    }

    @Test
    public void testIllegalMoveEast() {
        boolean atRHSide = false;
        try {
            myRobot.turn();
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atRHSide = myRobot.currentState().column == 10;
            assertTrue(atRHSide);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move East.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(10, myRobot.currentState().row);
        }
    }

    @Test
    public void testIllegalMoveSouth(){
        boolean atBottom = false;
            myRobot.turn();
            myRobot.turn();
            // Make the robot face south
            // Check that robot is now facing South
            atBottom = myRobot.currentState().direction == Robot.Direction.South;
            assertTrue(atBottom);
       try {
            // Now try to move South.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(10, myRobot.currentState().row);
        }
    }

    @Test
    public void testIllegalMoveWest(){
        boolean atLHSide = false;
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        // Make the robot face West
        // Check that robot is now facing West.
        atLHSide = myRobot.currentState().direction == Robot.Direction.West;
        assertTrue(atLHSide);
        try {
            // Now try to move West.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().column);
        }
    }


    @Test
    public void testMoveNorth() {
        try {
            //method itself in robot class throws exception so we have to surround with try catch block
            myRobot.move();
            assertEquals(9,myRobot.currentState().row);
        }
        catch (IllegalMoveException e){
            fail();
        }
    }

    @Test
    public void testMoveSouth() {
        try {
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();
            //move robot to the top of the grid
            myRobot.turn();
            myRobot.turn();
            //make it face south
            myRobot.move();
            //move south and check its position is as expected
            assertEquals(2,myRobot.currentState().row);
        }
        catch (IllegalMoveException e){
            fail();
        }
    }

    @Test
    public void testMoveEast() {
        try {
            myRobot.turn();
            myRobot.move();
            assertEquals(2,myRobot.currentState().column);
        }
        catch (IllegalMoveException e){
            fail();
        }
    }

    @Test
    public void testMoveWest() {
        try {
            myRobot.turn();
            myRobot.turn();
            myRobot.turn();
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            myRobot.turn();
            myRobot.move();
            assertEquals(2,myRobot.currentState().column);
        }
        catch (IllegalMoveException e){
            fail();
        }
    }


}
